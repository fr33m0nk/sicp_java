package sicp.problem1;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Solution1 {

    public int sumOfSquaresOfBiggestNumbers(List<Integer> seriesOfNumbers) {
        return seriesOfNumbers
                .stream()
                .sorted((x,y) -> y - x)
                .limit(2)
                .map(x -> x * x)
                .reduce(0, (x, y) -> x + y);
    }

    public int sumOfSquaresOfBiggestNumbers2(List<Integer> seriesOfNumbers) {
        return seriesOfNumbers
                .stream()
                .sorted((x,y) -> y - x)
                .limit(2)
                .map(x -> x * x)
                .reduce(0, Integer::sum);
    }

    public int sumOfSquaresOfBiggestNumbers3(List<Integer> seriesOfNumbers) {
        return seriesOfNumbers
                .stream()
                .sorted(Comparator.reverseOrder())
                .limit(2)
                .map(x -> x * x)
                .collect(Collectors.summingInt(Integer::intValue));
    }

    public int sumOfSquaresOfBiggestNumbers4(List<Integer> seriesOfNumbers) {
        return seriesOfNumbers
                .stream()
                .sorted(Comparator.reverseOrder())
                .limit(2)
                .mapToInt(x -> x * x)
                .sum();
    }

}

