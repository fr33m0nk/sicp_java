package sicp.problem2;

import java.util.stream.IntStream;

public class Solution2 {

    public int factorial(int n) {
        return IntStream
                .rangeClosed(1, n)
                .reduce(1,(a,b) -> a * b);
    }
}

