package sicp.problem2;

import org.junit.Test;

import static org.junit.Assert.*;

public class Solution2Test {
    private Solution2 solution;

    @Test
    public void factorial() {
        solution = new Solution2();
        assertEquals(24, solution.factorial(4));
        assertEquals(5040, solution.factorial(7));
    }
}