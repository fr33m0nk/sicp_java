package sicp.problem1;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class Solution1Test {

    private List<Integer> seriesOfNumber;
    private Solution1 solution;

    @Before
    public void setUp() {
        seriesOfNumber = Arrays.asList(1,56, 88, 20,1, 64, 0, 84);
    }

    @Test
    public void sumOfSquaresOfBiggestNumbers() {
        solution = new Solution1();
        int actualSum = solution.sumOfSquaresOfBiggestNumbers(seriesOfNumber);
        assertEquals(14800, actualSum);
    }

    @Test
    public void sumOfSquaresOfBiggestNumbers2() {
        solution = new Solution1();
        int actualSum = solution.sumOfSquaresOfBiggestNumbers2(seriesOfNumber);
        assertEquals(14800, actualSum);
    }

    @Test
    public void sumOfSquaresOfBiggestNumbers3() {
        solution = new Solution1();
        int actualSum = solution.sumOfSquaresOfBiggestNumbers3(seriesOfNumber);
        assertEquals(14800, actualSum);
    }

    @Test
    public void sumOfSquaresOfBiggestNumbers4() {
        solution = new Solution1();
        int actualSum = solution.sumOfSquaresOfBiggestNumbers4(seriesOfNumber);
        assertEquals(14800, actualSum);
    }
}